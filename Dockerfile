FROM ubuntu:latest

RUN apt-get update && apt-get install python3

COPY ./ /opt/registry

CMD [ "python3", "/opt/registry/main.py" ]